import React, { Fragment, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Menu } from '../../menu/menu'
import styles from './restaurant.module.css'
import { useLocation } from 'react-router-dom'
import { goodFoodApi } from '../../../shared/api'

// const menus = [
//   {
//     id: 1,
//     name: 'Bacon Burger',
//     description: 'Burger à base de bacon et de fromage',
//     price: 12.5,
//     img:
//       'https://img.cuisineaz.com/660x660/2016-05-21/i86551-burger-au-bacon-et-au-cheddar.jpg',
//   },
// ]

export const Restaurant = () => {
  const [restaurant, setRestaurant] = useState()
  const query = new URLSearchParams(useLocation().search)
  const id = query.get('id')

  useEffect(async () => {
    const result = await goodFoodApi.restaurants().getOneWithMenus(id)
    setRestaurant(result.data)
  }, [])
  return (
    <Fragment>
      <Link to={`/`}>
        <img
          className={styles.backButton}
          src="https://cdn.onlinewebfonts.com/svg/img_343367.png"
        />
      </Link>
      <div className={styles.container}>
        <div>
          <h1>Nom de restaurant</h1>
          <div className={styles.list}>
            {restaurant?.menus.map((menu) => (
              <Menu key={menu?._id} menu={menu} />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  )
}
