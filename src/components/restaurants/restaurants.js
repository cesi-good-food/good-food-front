import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Card } from '../card/card'
import styles from './restaurants.module.css'
import { goodFoodApi } from '../../shared/api'

// const restaurants = [
//   {
//     id: 1,
//     name: 'La bonne fourchette',
//     img: 'https://tinyurl.com/kcndesmc',
//     city: 'Anger',
//   },
//   {
//     id: 2,
//     name: 'Chez roger',
//     img:
//       'https://media-cdn.tripadvisor.com/media/photo-s/1a/18/3a/cb/restaurant-le-47.jpg',
//     city: 'Paris',
//   },
// ]

export const Restaurants = () => {
  const [restaurants, setRestaurants] = useState([])
  useEffect(async () => {
    const result = await goodFoodApi.restaurants().getAll()

    setRestaurants(result.data)
  }, [])

  return (
    <div className={styles.container}>
      <div className={styles.list}>
        {restaurants?.map((restaurant) => (
          <Link
            key={restaurant?._id}
            className={styles.link}
            to={`/restaurant?id=${restaurant?._id}`}
          >
            <Card restaurant={restaurant} />
          </Link>
        ))}
      </div>
    </div>
  )
}
