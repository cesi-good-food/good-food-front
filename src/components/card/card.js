import React from 'react'

import styles from './card.module.css'

export const Card = (props) => {
  console.log(props?.restaurant?.img)
  return (
    <div className={styles.card}>
      <img className={styles.image} src={props?.restaurant?.img} />
      <p className={styles.text}>
        {props?.restaurant?.name} - {props?.restaurant?.city}
      </p>
    </div>
  )
}
