import React from 'react'
import styles from './menu.module.css'

export const Menu = (props) => {
  console.log(props)
  return (
    <div className={styles.container}>
      <div>
        <h3>{props.menu.name}</h3>
        <div className={styles.list}>
          <p className={styles.description}>{props.menu.description}</p>
        </div>
      </div>
      <div className={styles.imgContainer}>
        <img className={styles.img} src={props.menu.img} />
      </div>
    </div>
  )
}
