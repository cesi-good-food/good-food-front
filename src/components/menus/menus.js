import React from 'react'
import { useLocation } from 'react-router-dom'
import styles from './menus.module.css'

export const Menus = () => {
  const query = new URLSearchParams(useLocation().search)
  const id = query.get('id')
  return (
    <div className={styles.container}>
      <div className={styles.list}>
        <p>{id}toto</p>
      </div>
    </div>
  )
}
