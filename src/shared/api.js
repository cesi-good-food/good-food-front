import axios from 'axios'

const API_URI = process.env.REACT_APP_API_URI

export const goodFoodApi = {
  restaurants() {
    return {
      getAll: () => axios.get(`${API_URI}/restaurants`),
      getOne: (id) => axios.get(`${API_URI}/restaurants/${id}`),
      getOneWithMenus: (id) =>
        axios.get(`${API_URI}/restaurants/${id}/withMenus`),
    }
  },
}
