import React from 'react'
import { Route, Switch } from 'react-router-dom'
import './App.css'
import { Header } from './components/header/header'
import { Restaurant } from './components/restaurants/restaurant/restaurant'
import { Restaurants } from './components/restaurants/restaurants'

const App = () => {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/restaurant">
          <Restaurant />
        </Route>
        <Route path="/">
          <Restaurants />
        </Route>
      </Switch>
    </div>
  )
}

export default App
